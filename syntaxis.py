# Mensaje por pantalla
print("Hola Mundo!!")

# Variables
saludo = "Hola"
nombre = "Bruno"
version = 3
print(saludo, nombre)
print(version)


def condiciones():
    numero = 8
    if numero < 0:
        print("negativo")
    elif numero > 0:
        print("positivo")
    else:
        print("cero")
    
    _numero = 0
    while(_numero <= 10):
        print("Numero:", _numero)
        _numero = _numero + 1

condiciones()

def mi_funcion(param1, param2):
    print(param1)
    print(param2)

mi_funcion(1,"hola")
mi_funcion("chao",2)