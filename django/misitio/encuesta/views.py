from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request, 'encuesta/index.html', {'mensaje': 'Hola mundo'})

def segundaPagina(request):
    return render(request, 'encuesta/segundaPagina.html')
