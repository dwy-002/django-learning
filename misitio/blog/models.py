from django.db import models
from django.utils import timezone

# Create your models here.
#https://docs.djangoproject.com/es/2.1/howto/initial-data/
class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    #file = models.FileField(blank=True, null=True, upload_to="blog")
    file = models.FileField(blank=True, null=True, upload_to="blog")


    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def create(self):
        self.save()
