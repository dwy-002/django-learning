from django.test import TestCase, Client
from .models import Post
from django.contrib.auth.models import User
from django.contrib.auth import *
from django.urls import reverse
from django.template import RequestContext
# Create your tests here.

class PostTest(TestCase):
    def setUp(self):
        user = User(username="user1", password="passowrd")
        user.save()

    def test_agregarPost(self):
        user = User.objects.get(username="user1")
        
        self.assertIsNotNone(user,"Exito")
        post = Post(title="Prueba test", author= user)
        post.save()
        self.assertIsNotNone(post,"Exito")

class ClientTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.user.save()

    def test_cliente(self):
        
        c = Client()
        response = c.post(reverse("blog:admin"), {'user': 'john', 'password': 'johnpassword'})
        self.assertEqual(response.status_code,302,"No iguales")
        self.assertRedirects(response,reverse("blog:index"))
    
    def test_sinacceso(self):
        c = Client()
        response = c.get(reverse("blog:create"))
        self.assertRedirects(response,reverse("blog:index"))

        